package gui;

import client.Client;
import javafx.fxml.FXML;


public class WindowController {

    private Client client;

    public void setClient(Client client) {
        this.client = client;
    }


    @FXML
    void buttonStop() {
        try{
            this.client.runCMD("stop");
        }catch (NullPointerException npe) {
            System.out.println("Not connected!");
        }
    }

    @FXML
    void buttonExit() {
        try{
            this.client.runCMD("exit");
        }catch (NullPointerException npe) {
            System.out.println("Not connected!");
        }
    }

    @FXML
    public void buttonSound() {
        try{
            this.client.runCMD("sound");
        }catch (NullPointerException npe) {
            System.out.println("Not connected!");
        }
    }

}
