package gui;

import client.Client;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class WindowView extends Application {

    Client client;

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("window.fxml"));
        Parent root = loader.load();
        connectToRobot();
        WindowController windowController = (WindowController) loader.getController();
        windowController.setClient(this.client);
        primaryStage.setTitle("LEGO Controller");
        Scene scene = new Scene(root, 600, 600);
        primaryStage.setScene(scene);

        scene.setOnKeyPressed(keyEvent -> {
            switch(keyEvent.getCode()) {
                case W:
                    client.runCMD("control:press:up");
                    break;
                case A:
                    client.runCMD("control:press:left");
                    break;
                case S:
                    client.runCMD("control:press:down");
                    break;
                case D:
                    client.runCMD("control:press:right");
                    break;
            }
        });
        scene.setOnKeyReleased(keyEvent -> {
            switch(keyEvent.getCode()) {
                case W:
                    client.runCMD("control:release:up");
                    break;
                case A:
                    client.runCMD("control:release:left");
                    break;
                case S:
                    client.runCMD("control:release:down");
                    break;
                case D:
                    client.runCMD("control:release:right");
                    break;
                case ESCAPE:
                    client.runCMD("exit");
                    break;
            }
        });
        primaryStage.show();
    }

    void connectToRobot() {
        this.client = new Client(3001, "10.0.1.1");
    }
}
