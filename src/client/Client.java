package client;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {

    private Socket client;
    private  int port;
    private String host;

    public Client(int port, String host) {
        this.host = host;
        this.port = port;

        try {
            this.client = new Socket(this.host, this.port);
            System.out.println("Client connected!");
        }catch(UnknownHostException ukhe) {
            System.out.println("No host with this IP found!");
        }catch(IOException ioe) {
            System.out.println("Fehler beim verbinden: "  + ioe.getMessage());
        }
    }

    public boolean runCMD(String cmd) {
       try {
           PrintStream ps = new PrintStream(this.client.getOutputStream(), true);
           ps.println(cmd);
           ps.flush();
           return true;
       }catch(IOException ioe) {
           System.out.println("Failed to send cmd: " + ioe.getMessage());
           return false;
       }
    }

    public Socket getClient() {
        return client;
    }

    public int getPort() {
        return port;
    }

    public String getHost() {
        return host;
    }
}
